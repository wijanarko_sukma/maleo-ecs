#include "Entity.h"

namespace MaleoECS 
{
	Entity::Entity (void) 
	{
	}

	Entity::~Entity (void)
	{
	}

	void Entity::init (void)
	{
		_bitComponentTypes = std::bitset<MAX_COMPONENTS>();
		this->reset();
	}

	void Entity::reset(void)
	{
		_iUniqueId = INVALID_ENTITY_UNIQUE_ID;
		_iOwnerUniqueId = INVALID_ENTITY_UNIQUE_ID;
		_eType = 0;
		_bitComponentTypes.reset();
	}

	uint32_t Entity::getUniqueId (void) 
	{
		return _iUniqueId;
	}

	void Entity::setUniqueId (uint32_t objectId) 
	{
		_iUniqueId = objectId;
	}

	uint32_t Entity::getOwnerUniqueId (void)
	{
		return _iOwnerUniqueId;
	}

	void Entity::setOwnerUniqueId (uint32_t ownerId) 
	{
		_iOwnerUniqueId = ownerId;
	}

	uint32_t Entity::getEntityType (void)
	{
		return _eType;
	}

	void Entity::setEntityType (uint32_t type)
	{
		_eType = type;
	}

	std::bitset<MAX_COMPONENTS> Entity::getComponentTypesBitField (void)
	{
		return _bitComponentTypes;
	}

	void Entity::setComponentTypesBitField (size_t posBit, bool val) 
	{
		_bitComponentTypes.set(posBit, val);
	}
}
