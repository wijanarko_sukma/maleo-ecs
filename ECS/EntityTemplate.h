#ifndef _MALEO_ECS_ENTITY_TEMPLATE_H_
#define _MALEO_ECS_ENTITY_TEMPLATE_H_

#include <iostream>
#include <stdint.h>
#include <vector>
#include <string>
#include "Component.h"

namespace MaleoECS {
	class EntityTemplate 
	{
	public:
		EntityTemplate (std::string name, uint32_t type);
		virtual ~EntityTemplate (void);

		bool addComponent (MaleoECS::Component * component);
		std::vector<MaleoECS::Component *> * getComponents (void);

		std::string getTemplateKey (void);
		uint32_t getTemplateType (void);
		void setTemplateType (uint32_t type);
	protected:
		std::string	_sName;
		uint32_t _eTemplateType;
		std::vector<MaleoECS::Component *> * _pComponents;
	};
}

#endif
