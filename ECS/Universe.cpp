#include "Universe.h"

static uint32_t	 TopLevelGroupUniqueIdBase = 0x40000000;

namespace MaleoECS {
	Universe::Universe (void)
	{
		 _iNextUniqueId = 0x00000001;
	}

	Universe::~Universe (void) 
	{
	}

	bool Universe::isTopLevelGroupId (uint32_t uniqueId)
	{
		// This does *not* include the global group.
		return uniqueId >= 0x40000000;
	}

	bool Universe::hasEntityOwner (uint32_t uniqueId)
	{
		return (uniqueId < 0x40000000) && (uniqueId != INVALID_ENTITY_UNIQUE_ID);
	}

	void Universe::resetUniqueId (void) 
	{
		_iNextUniqueId = 0x00000001;
	}

	uint32_t Universe::getNextUniqueId (void)
	{
		return this->_iNextUniqueId++;
	}

	uint32_t Universe::getTopLevelGroupUniqueIdBase (void) 
	{
		return TopLevelGroupUniqueIdBase;
	}
}