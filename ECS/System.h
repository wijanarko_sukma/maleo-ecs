#ifndef _MALEO_ECS_SYSTEM_H_
#define _MALEO_ECS_SYSTEM_H_

#include <iostream>
#include <stdint.h>
#include <vector>
#include <list>
#include <bitset>
#include "Core.h"
#include "Entity.h"

namespace MaleoECS {
	enum SystemFlags
	{
		NONE = 0x00000000,
		HANDLE_ALL_MSG = 0x00000001
	};

	class System 
	{
	public:
		System (void);
		virtual ~System (void);

		bool callInitialize (MaleoECS::EntityManager * em);
		virtual void addEntity (MaleoECS::Entity * pEntity);
		virtual void removeEntity (MaleoECS::Entity * entity);
		virtual void update (float dt) = 0;
		virtual void fixedUpdate (float dt) = 0;
		virtual int sendMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender);

		virtual MaleoECS::SystemFlags getFlags (void);
		virtual void setFlags (MaleoECS::SystemFlags flags);
		virtual bool isEnabled (void);
		virtual void setEnabled (bool enabled);
		virtual int getUpdateOrder (void);
		virtual void setUpdateOrder (int updateOrder);
		virtual std::vector<uint32_t> * getSupportedMessages (void);
		virtual void setSupportedMessages (std::vector<uint32_t> * supportedMessages);
		virtual std::bitset<MAX_COMPONENTS> getRequiredComponentTypesBitField (void);
		virtual void setRequiredComponentTypesBitField (size_t pos, bool val = false);
		virtual std::bitset<MAX_COMPONENTS> getRequiredAtLeastOneComponentTypesBitField (void);
		virtual void setRequiredAtLeastOneComponentTypesBitField (size_t pos, bool val = false);

	protected:
		MaleoECS::SystemFlags _eFlags;
		bool _isEnabled;
		MaleoECS::EntityManager * _pEntityManager;
		std::list<uint32_t> * _iEntityIds;
		int _iUpdateOrder;
		std::vector<uint32_t> * _pSupportedMessages;
		std::bitset<MAX_COMPONENTS> _bitRequiredComponentTypes;
		std::bitset<MAX_COMPONENTS> _bitRequiredAtLeastOneComponentTypes;

		void construct (int updateOrder, std::vector<uint32_t> * requiredComponentTypeIds, std::vector<uint32_t> * optionalButOneRequiredComponentTypeIds = NULL, std::vector<uint32_t> * supportedMessages = NULL, SystemFlags flags = SystemFlags::NONE);
	
		virtual bool initialize (void) = 0;
		virtual void onEnabledChanged (void) = 0;
		virtual void onEntityAdded (MaleoECS::Entity * entity, uint32_t uniqueId) = 0;
		virtual void onEntityRemoved (MaleoECS::Entity * entity, uint32_t uniqueId) = 0;
		virtual int onHandleMessage (MaleoECS::Entity * entity, uint32_t messageId, void * data, void * sender) = 0;
	};
}

#endif
