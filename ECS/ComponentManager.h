#ifndef _MALEO_ECS_COMPONENT_MANAGER_H_
#define _MALEO_ECS_COMPONENT_MANAGER_H_

#include <iostream>
#include <stdint.h>
#include <map>
#include "Core.h"
#include "Component.h"
#include "Entity.h"

namespace MaleoECS {
	class IComponentManagerBase 
	{
	public:
		virtual short elementsLeft (void) = 0;
		virtual bool isComponentAllocated (MaleoECS::Entity * entity) = 0;
		virtual bool isComponentAllocated (uint32_t uniqueId) = 0;
		virtual MaleoECS::Component * getComponent (MaleoECS::Entity * entity) = 0;
		virtual MaleoECS::Component * getComponent (uint32_t uniqueId) = 0;
		virtual MaleoECS::Component * allocateComponent (MaleoECS::Entity * entity) = 0;
		virtual MaleoECS::Component * allocateComponent (uint32_t uniqueId) = 0;
		virtual void restoreComponent (MaleoECS::Entity * entity) = 0;
		virtual void restoreComponent (uint32_t uniqueId) = 0;
		virtual void setEntityManager (MaleoECS::EntityManager * em) = 0;
	};


	template<class T>
	class IComponentMapper
	{
	public:
		virtual T * getComponentFor (MaleoECS::Entity * entity) = 0;
		virtual T * getComponentFor (uint32_t uniqueId) = 0;
		virtual std::map<uint32_t, T *> * enumerateComponents (void) = 0;
	};


	template<class T>
	class ComponentManager : public IComponentManagerBase, public IComponentMapper<T> 
	{
	public:
		ComponentManager (uint32_t componentTypeId, short defaultPoolSize) :
			_iPoolSize(0),
			_iElementsCount(0)
		{
			_pComponentPool = new std::vector<T *>();
			_pUniqueIdToComponent = new std::map<uint32_t, T *>();
			if(defaultPoolSize > 0) {
				growPool(defaultPoolSize);
			}

			_iComponentTypeId = componentTypeId;
		}

		virtual ~ComponentManager (void) 
		{
			_pEntityManager = nullptr;
			_pComponentPool->clear();
			_pUniqueIdToComponent->clear();

			delete _pComponentPool;
			delete _pUniqueIdToComponent;
			_pComponentPool = nullptr;
			_pUniqueIdToComponent = nullptr;
		}

		void setEntityManager (MaleoECS::EntityManager * em) 
		{
			_pEntityManager = em;
		}

		void growPool (short poolSize) 
		{
			_ASSERT(poolSize > _iPoolSize);

			short oldSize = _iPoolSize;
			short newSize = (short)MAX(_iPoolSize * 2, poolSize);
			short diffSize = newSize - oldSize;

			for(int i = oldSize; i < newSize; i++) {
				T * data = new T();
				_pComponentPool->push_back(data);
			}
	
			_iPoolSize = newSize;
			_iElementsCount += diffSize;
		}

		short elementsLeft (void)
		{
			return _iElementsCount;
		}

		bool isComponentAllocated (MaleoECS::Entity * entity)
		{
			return this->isComponentAllocated(entity->getUniqueId());
		}

		bool isComponentAllocated (uint32_t uniqueId)
		{
			bool result = (_pUniqueIdToComponent->find(uniqueId) != _pUniqueIdToComponent->end());
			return result;
		}

		T * getComponent (MaleoECS::Entity * entity) 
		{
			return this->getComponent(entity->getUniqueId());
		}

		T * getComponent (uint32_t uniqueId)
		{
			T * result = _pUniqueIdToComponent->at(uniqueId);
			return result;
		}

		T * allocateComponent (MaleoECS::Entity * entity) 
		{
			return this->allocateComponent(entity->getUniqueId());
		}

		T * allocateComponent (uint32_t uniqueId)
		{
			if (_iElementsCount == 0) {
				growPool((short)(_iPoolSize + 1)); // Actually, it will double.
			}

			T * result = _pComponentPool->back();
			_pComponentPool->pop_back();
			//Component * result = dynamic_cast<Component *>data;
			result->init();
			_iElementsCount--;

			_pUniqueIdToComponent->insert(_pUniqueIdToComponent->begin(), std::pair<uint32_t, T *>(uniqueId, result));
			return result;
		}

		void restoreComponent (MaleoECS::Entity * entity)
		{
			this->restoreComponent(entity->getUniqueId());
		}

		void restoreComponent (uint32_t uniqueId) 
		{
			T * component = _pUniqueIdToComponent->at(uniqueId);
			_pComponentPool->push_back((T *)component);
			_iElementsCount++;
			_pUniqueIdToComponent->erase(uniqueId);
		}

		T * getComponentFor (MaleoECS::Entity * entity) 
		{
			return this->getComponentFor(entity->getUniqueId());
		}

		T * getComponentFor (uint32_t uniqueId)
		{
			T * result = _pUniqueIdToComponent->at(uniqueId);
			return result;
		}

		std::map<uint32_t, T *> *  enumerateComponents (void)
		{
			return _pUniqueIdToComponent;
		}
	protected:
		MaleoECS::EntityManager * _pEntityManager;
		int	_iComponentTypeId;
		std::vector<T *> * _pComponentPool;
		std::map<uint32_t, T *> * _pUniqueIdToComponent;
		short _iPoolSize;
		short _iElementsCount;
	};
}

#endif