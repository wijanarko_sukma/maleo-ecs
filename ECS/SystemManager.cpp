#include "SystemManager.h"
#include "EntityManager.h"
#include "MessageData.h"

namespace MaleoECS {
	SystemManager::SystemManager (MaleoECS::EntityManager * em) 
	{
		_pEntityManager = em;
		_pEntityManager->setSystemManager(this);
		_pSystems = new std::map<uint32_t, MaleoECS::System *>();
		_pMessagesSystemDispatch = new std::map<uint32_t, MaleoECS::System *>();
		_pGlobalMessageHandlers = new std::vector<MaleoECS::System *>();
	}

	SystemManager::~SystemManager (void) 
	{
		_pGlobalMessageHandlers->clear();
		_pMessagesSystemDispatch->clear();
		_pSystems->clear();

		delete _pGlobalMessageHandlers;
		delete _pMessagesSystemDispatch;
		delete _pSystems;
		_pEntityManager = nullptr;
		_pGlobalMessageHandlers = nullptr;
		_pMessagesSystemDispatch = nullptr;
		_pSystems = nullptr;
	}

	void SystemManager::addSystem (MaleoECS::System * system)
	{
		system->callInitialize(_pEntityManager);
		_pSystems->insert(std::pair<uint32_t, MaleoECS::System *>(system->getUpdateOrder(), system));

		if (system->getSupportedMessages() != nullptr) {
			for (std::vector<uint32_t>::iterator it = system->getSupportedMessages()->begin(); it != system->getSupportedMessages()->end(); ++it) {
				uint32_t message = *it;
				if (_pMessagesSystemDispatch->find(message) == _pMessagesSystemDispatch->end()) {
					_pMessagesSystemDispatch->insert(std::pair<uint32_t, MaleoECS::System *>(message, system));
				}
			}
		}

		if ((system->getFlags() & MaleoECS::SystemFlags::HANDLE_ALL_MSG) == MaleoECS::SystemFlags::HANDLE_ALL_MSG) {
			_pGlobalMessageHandlers->push_back(system);
		}
	}

	std::map<uint32_t, MaleoECS::System *> * SystemManager::getSystems (void)
	{
		return _pSystems;
	}

	void SystemManager::updateSystems (float dt) 
	{
		for (auto it = _pSystems->begin(); it != _pSystems->end(); ++it) {
			if (it->second->isEnabled())
				it->second->update(dt);
		}
	}

	void SystemManager::fixedUpdateSystems(float dt)
	{
		for (auto it = _pSystems->begin(); it != _pSystems->end(); ++it) {
			if (it->second->isEnabled())
				it->second->fixedUpdate(dt);
		}
	}

	int SystemManager::sendMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender)
	{
		int result = 0;
		MaleoECS::System * system = NULL;
		MaleoECS::MessageData * msg = (MaleoECS::MessageData *)data;
		std::map<uint32_t, MaleoECS::System *>::iterator iter = _pMessagesSystemDispatch->find(messageId);
		
		if (iter != _pMessagesSystemDispatch->end()) {
			system = iter->second;
			result = system->sendMessage(target, messageId, data, sender);
		}

		// We now allow for global message handlers
		if (!msg->isHandled()) {
			for (std::vector<MaleoECS::System *>::iterator iter = _pGlobalMessageHandlers->begin(); iter != _pGlobalMessageHandlers->end(); ++iter) {
				(*iter)->sendMessage(target, messageId, data, sender);
				MaleoECS::MessageData * msg2 = (MaleoECS::MessageData *)data;

				if (msg2->isHandled()) {
					break;
				}
			}
		}
		return result;
	}
}
