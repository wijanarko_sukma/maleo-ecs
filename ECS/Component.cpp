#include "Component.h"

namespace MaleoECS 
{
	Component::Component (void) 
	{
		_isActive = false;
	}

	Component::~Component (void) 
	{
	}

	void Component::copy (MaleoECS::Component * component) 
	{
		_isActive = component->isActive();
	}

	bool Component::isActive (void) 
	{
		return _isActive;
	}

	bool Component::isInitialized (void) 
	{
		return _isInitialized;
	}

	void Component::setInitialized (bool initialized) 
	{
		_isInitialized = initialized;
	}

	uint32_t Component::getComponentType (void)
	{
		return _iComponentType;
	}

	void Component::setComponentType (uint32_t type) 
	{
		_iComponentType = type;
	}
}