#include "EntityTemplateManager.h"

static MaleoECS::EntityTemplateManager * _pInstance = nullptr;

namespace MaleoECS
{
	EntityTemplateManager::EntityTemplateManager (void) 
	{
		_pEntityTemplates = new std::map<std::string, EntityTemplate *>();
	}

	EntityTemplateManager::~EntityTemplateManager (void)
	{
		_pEntityTemplates->clear();
		delete _pEntityTemplates;
		_pEntityTemplates = nullptr;
	}

	MaleoECS::EntityTemplateManager * EntityTemplateManager::getInstance (void)
	{
		if(_pInstance == nullptr) {
			_pInstance = new MaleoECS::EntityTemplateManager();
		}

		return _pInstance;
	}

	bool EntityTemplateManager::registerTemplate (MaleoECS::EntityTemplate * entity) 
	{
		bool result = false;

		if (this->isTemplateExist(entity->getTemplateKey()))
			return false;

		_pEntityTemplates->insert(std::pair<std::string, MaleoECS::EntityTemplate *>(entity->getTemplateKey(), entity));
		return true;
	}

	bool EntityTemplateManager::isTemplateExist (std::string key) 
	{
		if (_pEntityTemplates->count(key) == 1)
			return true;

		return false;
	}

	MaleoECS::EntityTemplate * EntityTemplateManager::getTemplate (std::string key) 
	{
		if (this->isTemplateExist(key))
			return _pEntityTemplates->at(key);
		return nullptr;
	}
}