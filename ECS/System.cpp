#include "System.h"
#include "Component.h"

namespace MaleoECS {
	System::System (void) :
		_isEnabled(false),
		_eFlags(MaleoECS::SystemFlags::NONE),
		_iUpdateOrder(0),
		_pEntityManager(nullptr),
		_pSupportedMessages(nullptr)
	{
		_iEntityIds = new std::list<uint32_t>();
		_bitRequiredComponentTypes = std::bitset<MAX_COMPONENTS>();
		_bitRequiredAtLeastOneComponentTypes = std::bitset<MAX_COMPONENTS>();
	}

	System::~System (void) 
	{
		_iEntityIds->clear();
		_bitRequiredComponentTypes.reset();
		_bitRequiredAtLeastOneComponentTypes.reset();
		delete _iEntityIds;
		_iEntityIds = nullptr;
	}

	void System::construct (int updateOrder, std::vector<uint32_t> * requiredComponentTypeIds, std::vector<uint32_t> * optionalButOneRequiredComponentTypeIds, std::vector<uint32_t> * supportedMessages, SystemFlags flags)
	{
		_eFlags = flags;
		_iUpdateOrder = updateOrder;
		_isEnabled = true;
		for(auto iter = requiredComponentTypeIds->begin(); iter != requiredComponentTypeIds->end(); ++iter) {
			uint32_t componentType = (*iter);
			_bitRequiredComponentTypes.set(componentType);
		}

		if (optionalButOneRequiredComponentTypeIds != nullptr)	{
			for(auto iter = optionalButOneRequiredComponentTypeIds->begin(); iter != optionalButOneRequiredComponentTypeIds->end(); ++iter) {
				uint32_t componentType = (*iter);
				_bitRequiredAtLeastOneComponentTypes.set(componentType);
			}
		}
		_pSupportedMessages = supportedMessages;
	}

	bool System::callInitialize (MaleoECS::EntityManager * em)
	{
		_pEntityManager = em;
		return this->initialize();
	}

	MaleoECS::SystemFlags System::getFlags (void) 
	{
		return _eFlags;
	}

	void System::setFlags (MaleoECS::SystemFlags flags)
	{
		_eFlags = flags;
	}

	bool System::isEnabled (void)
	{
		return _isEnabled;
	}

	void System::setEnabled (bool enabled)
	{
		if(_isEnabled != enabled) {
			_isEnabled = enabled;
			this->onEnabledChanged();
		}
	}

	int System::getUpdateOrder (void)
	{
		return _iUpdateOrder;
	}

	void System::setUpdateOrder (int updateOrder)
	{
		_iUpdateOrder = updateOrder;
	}

	std::vector<uint32_t> * System::getSupportedMessages (void)
	{
		return _pSupportedMessages;
	}

	void System::setSupportedMessages (std::vector<uint32_t> * supportedMsgs) 
	{
		_pSupportedMessages = supportedMsgs;
	}

	std::bitset<MAX_COMPONENTS> System::getRequiredComponentTypesBitField (void)
	{
		return _bitRequiredComponentTypes;
	}

	void System::setRequiredComponentTypesBitField (size_t pos, bool val)
	{
		_bitRequiredComponentTypes.set(pos, val);
	}

	std::bitset<MAX_COMPONENTS> System::getRequiredAtLeastOneComponentTypesBitField (void) 
	{
		return _bitRequiredAtLeastOneComponentTypes;
	}

	void System::setRequiredAtLeastOneComponentTypesBitField (size_t pos, bool val) 
	{
		_bitRequiredAtLeastOneComponentTypes.set(pos, val);
	}

	void System::addEntity (MaleoECS::Entity * entity)
	{
		_iEntityIds->push_back(entity->getUniqueId());

		this->onEntityAdded(entity, entity->getUniqueId());
	}

	void System::removeEntity (MaleoECS::Entity * entity) 
	{
		this->onEntityRemoved(entity, entity->getUniqueId());

		_iEntityIds->remove(entity->getUniqueId());
	}

	int System::sendMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender) 
	{
		int result = 0;
	
		if(_isEnabled) {
			for(std::list<uint32_t>::iterator iter = _iEntityIds->begin(); iter != _iEntityIds->end(); ++iter) {
				int uniqueId = *iter;
				if(uniqueId != target->getUniqueId())
					continue;

				result = this->onHandleMessage(target, messageId, data, sender);
				break;
			}
		}

		return result;
	}
}