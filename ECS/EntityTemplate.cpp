#include "EntityTemplate.h"

namespace MaleoECS {
	EntityTemplate::EntityTemplate(std::string name, uint32_t type) :
		_sName (name),
		_eTemplateType (type)
	{
		_pComponents = new std::vector<MaleoECS::Component *>();
	}

	EntityTemplate::~EntityTemplate (void) 
	{
		_pComponents->clear();
		delete _pComponents;
		_pComponents = nullptr;
	}

	bool EntityTemplate::addComponent (MaleoECS::Component * component) 
	{
		bool result = false;
	
		if(std::find(_pComponents->begin(), _pComponents->end(), component) == _pComponents->end()) {
			_pComponents->push_back(component);
			result = true;
		}

		return result;
	}

	std::string EntityTemplate::getTemplateKey (void) 
	{
		return _sName;
	}

	uint32_t EntityTemplate::getTemplateType (void)
	{
		return _eTemplateType;
	}

	void EntityTemplate::setTemplateType(uint32_t type)
	{
		_eTemplateType = type;
	}

	std::vector<MaleoECS::Component *> * EntityTemplate::getComponents (void)
	{
		return _pComponents;
	}
}
