#include "EntityManager.h"
#include <bitset>
#include "Core.h"
#include "Component.h"
#include "MessageData.h"

namespace MaleoECS {
	EntityManager::EntityManager (MaleoECS::Universe * universe, int maxEntities)
	{
		_pUniverse = universe;
		_pComponentManagers = new std::map<uint32_t, MaleoECS::IComponentManagerBase *>();
		_pEntityPool = new std::vector<MaleoECS::Entity *>(maxEntities);
		_iUniqueIdToEntity = new std::map<uint32_t, MaleoECS::Entity *>();
		_iEntityToBeRemoved = new std::vector<uint32_t>();

		this->initialize(maxEntities);
	}

	EntityManager::~EntityManager (void) 
	{
		_pEntityPool->clear();
		_iUniqueIdToEntity->clear();
		_pComponentManagers->clear();
		_iEntityToBeRemoved->clear();
		delete _pEntityPool;
		delete _iUniqueIdToEntity;
		delete _pComponentManagers;
		delete _iEntityToBeRemoved;
		delete _pSystemManager;

		_pEntityPool = nullptr;
		_iUniqueIdToEntity = nullptr;
		_pComponentManagers = nullptr;
		_iEntityToBeRemoved = nullptr;
		_pSystemManager = nullptr;
	}

	bool EntityManager::initialize (int maxEntities)
	{
		for (int i = 0; i < maxEntities; i++) {
			(*_pEntityPool)[i] = new Entity();
		}

		return true;
	}

	MaleoECS::IComponentManagerBase * EntityManager::getBaseComponentManager (uint32_t componentType)
	{
		return _pComponentManagers->at(componentType);
	}

	void EntityManager::updateSystems (float dt) 
	{
		_pSystemManager->updateSystems(dt);
	}

	void EntityManager::fixedUpdateSystems (float dt)
	{
		_pSystemManager->fixedUpdateSystems(dt);
	}

	void EntityManager::postUpdateSystems (float dt)
	{
		for (auto iter = _iEntityToBeRemoved->begin(); iter != _iEntityToBeRemoved->end(); ++iter) {
			uint32_t uniqueId = (*iter);
			this->removeEntity(_iUniqueIdToEntity->at(uniqueId));
		}
		_iEntityToBeRemoved->clear();
	}

	int EntityManager::sendMessage (MaleoECS::Entity * entity, uint32_t messageId, void * sender)
	{
		MessageData * data = new MessageData();
		return this->sendMessage(entity, messageId, data, sender);
	}

	int EntityManager::sendMessage (MaleoECS::Entity * entity, uint32_t messageId, void * data, void * sender)
	{
		return this->sendMessage(entity->getUniqueId(), messageId, data, sender);
	}

	int EntityManager::sendMessage (uint32_t uniqueId, uint32_t messageId, void * data, void * sender) 
	{
		int result = 0;
		Entity * target = NULL;

		if (uniqueId == INVALID_ENTITY_UNIQUE_ID) {
			result = this->deliverMessage(NULL, messageId, data, sender);
		} else {
			auto iter = _iUniqueIdToEntity->find(uniqueId);
			if (iter != _iUniqueIdToEntity->end()) {
				target = iter->second;
				result = deliverMessage(target, messageId, data, sender);
			}
		}

		return result;
	}

	int EntityManager::deliverMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender) 
	{
		int result = _pSystemManager->sendMessage(target, messageId, data, sender);

		MaleoECS::MessageData * msg = (MaleoECS::MessageData *)data;
		if (!msg->isHandled()) {
			if (target != nullptr) {

			}
		}

		return result;
	}

	MaleoECS::Entity * EntityManager::createEntity (std::string key, uint32_t ownerId)
	{
		return this->createEntity(MaleoECS::EntityTemplateManager::getInstance()->getTemplate(key), ownerId);
	}

	MaleoECS::Entity * EntityManager::createEntity (MaleoECS::EntityTemplate * temp, uint32_t ownerId)
	{
		MaleoECS::Entity * entity = this->allocateEntity(_pUniverse->getNextUniqueId());
		entity->setEntityType(temp->getTemplateType());
		std::vector<MaleoECS::Component *> * components = temp->getComponents();
		for(auto iter = components->begin(); iter != components->end(); ++iter) {
			MaleoECS::Component * componentTemplate= *iter;
			MaleoECS::Component * component;
			MaleoECS::IComponentManagerBase * manager = this->getBaseComponentManager(componentTemplate->getComponentType());
			if(! manager->isComponentAllocated(entity)) {
				component = manager->allocateComponent(entity);
				component->copy(componentTemplate);
				entity->setComponentTypesBitField(componentTemplate->getComponentType(), true);
			} else {
				component = manager->getComponent(entity);
			}
		}

		this->addEntityToSystems(entity);

		return entity;
	}

	void EntityManager::registerEntityToBeRemoved(uint32_t uniqueId)
	{
		_iEntityToBeRemoved->push_back(uniqueId);
	}

	void EntityManager::removeEntity (MaleoECS::Entity * entity) 
	{
		this->removeEntityFromSystems(entity);
		std::bitset<MAX_COMPONENTS> entityComponentBit = entity->getComponentTypesBitField();
	
		for (auto iter = _pComponentManagers->begin(); iter != _pComponentManagers->end(); ++iter) {
			uint32_t componentType = iter->first;

			if (!entityComponentBit.at(componentType))
				continue;

			MaleoECS::IComponentManagerBase * manager = this->getBaseComponentManager(componentType);
			manager->restoreComponent(entity);
		}
		this->restoreEntity(entity);
	}

	void EntityManager::freeAllEntities (void) 
	{

	}

	
	MaleoECS::Entity * EntityManager::allocateEntity (uint32_t uniqueId)
	{
		MaleoECS::Entity * entity = _pEntityPool->back();
		_pEntityPool->pop_back();

		entity->init();
		entity->setUniqueId(uniqueId);
		_iUniqueIdToEntity->insert(std::pair<uint32_t, MaleoECS::Entity *>(uniqueId, entity));
		return entity;
	}

	void EntityManager::restoreEntity (MaleoECS::Entity * entity) 
	{
		_iUniqueIdToEntity->erase(entity->getUniqueId());
		entity->reset();
		_pEntityPool->push_back(entity);
	}

	void EntityManager::addEntityToSystems (MaleoECS::Entity * entity) 
	{
		for(auto iter = _pSystemManager->getSystems()->begin(); iter != _pSystemManager->getSystems()->end(); ++iter) {
			auto system = iter->second;
			bool shouldBeInSystem = ((system->getRequiredComponentTypesBitField() & entity->getComponentTypesBitField()) == system->getRequiredComponentTypesBitField());
			shouldBeInSystem = shouldBeInSystem && ((system->getRequiredAtLeastOneComponentTypesBitField() & entity->getComponentTypesBitField()) != 0);
			if(shouldBeInSystem) {
				system->addEntity(entity);
			}
		}
	}

	void EntityManager::removeEntityFromSystems (MaleoECS::Entity * entity)
	{
		for (auto iter = _pSystemManager->getSystems()->begin(); iter != _pSystemManager->getSystems()->end(); ++iter) {
			auto system = iter->second;
			bool shouldBeInSystem = ((system->getRequiredComponentTypesBitField() & entity->getComponentTypesBitField()) == system->getRequiredComponentTypesBitField());
			shouldBeInSystem = shouldBeInSystem && ((system->getRequiredAtLeastOneComponentTypesBitField() & entity->getComponentTypesBitField()) != 0);
			if (shouldBeInSystem) {
				system->removeEntity(entity);
			}
		}
	}

	MaleoECS::SystemManager * EntityManager::getSystemManager (void)
	{
		return _pSystemManager;
	}

	void EntityManager::setSystemManager (MaleoECS::SystemManager * mgr)
	{
		_pSystemManager = mgr;
	}
}