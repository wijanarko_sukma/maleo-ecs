#ifndef _MALEO_ECS_CORE_H_
#define _MALEO_ECS_CORE_H_

#include <iostream>

#define INVALID_ENTITY_UNIQUE_ID -1
#define INVALID_ENTITY_LIVE_ID -1
#define MAX_COMPONENTS 50

namespace MaleoECS {
	class EntityManager;
}

#endif