#ifndef _MALEO_ECS_COMPONENT_H_
#define _MALEO_ECS_COMPONENT_H_

#include <iostream>
#include <stdint.h>
#include "Core.h"

namespace MaleoECS {
	class ComponentTypeIdHelper 
	{
	public:
		static int getBit (uint32_t componentTypeId)
		{
			return 0x1 << componentTypeId;
		}

		static const int AllMask = 0x7fffffff;
	};

	class Component 
	{
	public:
		Component (void);
		virtual ~Component (void);
	
		virtual bool init (void) = 0;
		virtual void shutDown (void) = 0;
		virtual MaleoECS::Component * clone (void) = 0;
		virtual void copy (MaleoECS::Component * component);
		bool isActive (void);
		
		virtual bool isInitialized (void);
		virtual void setInitialized (bool initialized);
		virtual uint32_t getComponentType (void);
		virtual void setComponentType (uint32_t type);
	protected:
		bool _isActive;
		bool _isInitialized;
		uint32_t _iComponentType;
	};
}

#endif
