#ifndef _MALEO_ECS_ENTITY_TEMPLATE_MANAGER_H_
#define _MALEO_ECS_ENTITY_TEMPLATE_MANAGER_H_

#include <iostream>
#include <stdint.h>
#include <map>
#include "EntityTemplate.h"

namespace MaleoECS {
	class EntityTemplateManager
	{
	public:
		virtual ~EntityTemplateManager (void);

		static MaleoECS::EntityTemplateManager * getInstance (void);
		bool registerTemplate (MaleoECS::EntityTemplate * entity);
		bool isTemplateExist (std::string key);
		MaleoECS::EntityTemplate * getTemplate (std::string key);

	private:
		std::map<std::string, MaleoECS::EntityTemplate *> * _pEntityTemplates;

		EntityTemplateManager (void);
	};
}

#endif