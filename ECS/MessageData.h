#ifndef _MALEO_ECS_MESSAGE_DATA_H_
#define _MALEO_ECS_MESSAGE_DATA_H_

#include <iostream>

namespace MaleoECS {
	struct MessageData 
	{
	public:
		MessageData (void) 
		{
			_isHandled = false;
		}

		bool isHandled (void)
		{
			return _isHandled;
		}

		void setHandled (bool handled) 
		{
			_isHandled = handled;
		}

	protected:
		bool _isHandled;
	};
}

#endif