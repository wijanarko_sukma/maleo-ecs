#ifndef _MALEO_ECS_UNIVERSE_H_
#define _MALEO_ECS_UNIVERSE_H_

#include <iostream>
#include <stdint.h>
#include "Core.h"

namespace MaleoECS {
	class Universe
	{
	public:
		Universe (void);
		virtual ~Universe (void);

		static uint32_t getTopLevelGroupUniqueIdBase (void);
		static bool isTopLevelGroupId (uint32_t uniqueId);
		static bool hasEntityOwner (uint32_t uniqueId);
		void resetUniqueId (void);
		uint32_t getNextUniqueId (void);
	private:
		uint32_t _iNextUniqueId;
	};
}

#endif