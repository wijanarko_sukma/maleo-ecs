#ifndef _MALEO_ECS_ENTITY_MANAGER_H_
#define _MALEO_ECS_ENTITY_MANAGER_H_

#include <iostream>
#include <stdint.h>
#include "Core.h"
#include "Universe.h"
#include "SystemManager.h"
#include "ComponentManager.h"
#include "EntityTemplateManager.h"
#include "Entity.h"
#include "EntityTemplate.h"

namespace MaleoECS {
	class EntityManager 
	{
	public:
		EntityManager (MaleoECS::Universe * universe, int maxEntities);
		virtual ~EntityManager (void);

		bool initialize (int maxEntities);

		template <class T>
		void addComponentType (uint32_t componentType, short defaultSize) 
		{
			_pComponentManagers->insert(std::pair<uint32_t, MaleoECS::IComponentManagerBase*>(componentType, this->createManager<T>(componentType, defaultSize)));
			//m_pTypeToComponentId->insert(std::pair<EComponentType, int>(componentType, componentType));
		}

		template <class T>
		MaleoECS::ComponentManager<T> * getComponentManager (uint32_t componentType)
		{
			return (MaleoECS::ComponentManager<T> *)_pComponentManagers->at(componentType);
		}

		MaleoECS::IComponentManagerBase * getBaseComponentManager (uint32_t componentType);

		template <class T>
		T * getComponent (MaleoECS::Entity * entity, uint32_t componentType)
		{
			uint32_t uniqueId = entity->getUniqueId();
			return this->getComponent<T>(uniqueId, componentType);
		}

		template <class T>
		T * getComponent (uint32_t uniqueId, uint32_t componentType) 
		{
			//int componentTypeLookupId = m_iComponentLookupIdsPerEntity->at(entityLiveId + componentTypeId * MAX_ENTITIES);
			T * result = this->getComponentManager<T>(componentType)->getComponent(uniqueId);
			return result;
		}

		Entity * createEntity (std::string key, uint32_t ownerId);
		void registerEntityToBeRemoved (uint32_t uniqueId);
		void freeAllEntities (void);
		void updateSystems (float dt);
		void fixedUpdateSystems (float dt);
		void postUpdateSystems (float dt);

		int sendMessage (MaleoECS::Entity * entity, uint32_t messageId, void * sender);
		int sendMessage (MaleoECS::Entity * entity, uint32_t messageId, void * data, void * sender);
		int sendMessage (uint32_t uniqueId, uint32_t message, void * data, void * sender);
		int deliverMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender);

		SystemManager * getSystemManager (void);
		void setSystemManager (MaleoECS::SystemManager * mgr);
	protected:
		MaleoECS::Universe * _pUniverse;
		MaleoECS::SystemManager * _pSystemManager;
		std::map<uint32_t, MaleoECS::IComponentManagerBase *> * _pComponentManagers;
		std::vector<MaleoECS::Entity *> * _pEntityPool;
		std::map<uint32_t, MaleoECS::Entity *> * _iUniqueIdToEntity;
		std::vector<uint32_t> * _iEntityToBeRemoved;

		template <class T>
		MaleoECS::IComponentManagerBase *  createManager (uint32_t componentTypeId, short defaultSize)
		{
			MaleoECS::IComponentManagerBase * result = NULL;
			result = new MaleoECS::ComponentManager<T>(componentTypeId, defaultSize);

			result->setEntityManager(this);
			return result;
		}

		Entity * allocateEntity(uint32_t uniqueId);
		void restoreEntity (MaleoECS::Entity * entity);
		Entity * createEntity(MaleoECS::EntityTemplate * temp, uint32_t ownerId);
		void removeEntity(MaleoECS::Entity * entity);
		void addEntityToSystems (MaleoECS::Entity * entity);
		void removeEntityFromSystems(MaleoECS::Entity * entity);
	};
}

#endif
