#ifndef _MALEO_ECS_SYSTEM_MANAGER_H_
#define _MALEO_ECS_SYSTEM_MANAGER_H_

#include <stdio.h>
#include <stdint.h>
#include <map>
#include <vector>
#include "Core.h"
#include "Entity.h"
#include "System.h"

namespace MaleoECS {
	class SystemManager 
	{
	public:
		SystemManager (MaleoECS::EntityManager * em);
		virtual ~SystemManager (void);

		void addSystem (MaleoECS::System * system);
		void updateSystems (float dt);
		void fixedUpdateSystems (float dt);
		int sendMessage (MaleoECS::Entity * target, uint32_t messageId, void * data, void * sender);
		std::map<uint32_t, MaleoECS::System *> * getSystems(void);

	protected:
		MaleoECS::EntityManager * _pEntityManager;
		std::map<uint32_t, MaleoECS::System *> * _pSystems;
		std::map<uint32_t, MaleoECS::System *> * _pMessagesSystemDispatch;
		std::vector<MaleoECS::System *> * _pGlobalMessageHandlers;
	};
}

#endif