#ifndef _MALEO_ECS_ENTITY_H_
#define _MALEO_ECS_ENTITY_H_

#include <iostream>
#include <stdint.h>
#include <bitset>
#include "Core.h"

namespace MaleoECS 
{
	class Entity 
	{
	public:
		Entity (void);
		virtual ~Entity (void);
		void init (void);

		void reset (void);
		virtual uint32_t getUniqueId (void);
		virtual void setUniqueId (uint32_t objectId);
		virtual uint32_t getOwnerUniqueId (void);
		virtual void setOwnerUniqueId (uint32_t ownerId);
		virtual uint32_t getEntityType (void);
		virtual void setEntityType (uint32_t type);
		virtual std::bitset<MAX_COMPONENTS> getComponentTypesBitField (void);
		virtual void setComponentTypesBitField (size_t posBit, bool val);

	protected:
		uint32_t _iUniqueId;
		uint32_t _iOwnerUniqueId;
		uint32_t _eType;
		std::bitset<MAX_COMPONENTS> _bitComponentTypes;
	};
}

#endif