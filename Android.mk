LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := maleoecs_static

LOCAL_MODULE_FILENAME := libmaleoecs

LOCAL_SRC_FILES := \
ECS/Universe.cpp \
ECS/Entity.cpp \
ECS/Component.cpp \
ECS/System.cpp \
ECS/EntityTemplate.cpp \
ECS/SystemManager.cpp \
ECS/EntityTemplateManager.cpp \
ECS/EntityManager.cpp

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/..

LOCAL_C_INCLUDES := $(LOCAL_PATH)/..
                                 
include $(BUILD_STATIC_LIBRARY)
