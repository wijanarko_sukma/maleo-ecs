#ifndef _MALEO_ECS_H_
#define _MALEO_ECS_H_

#include "ECS\Core.h"
#include "ECS\Universe.h"
#include "ECS\EntityTemplate.h"
#include "ECS\Entity.h"
#include "ECS\Component.h"
#include "ECS\System.h"
#include "ECS\EntityTemplateManager.h"
#include "ECS\EntityManager.h"
#include "ECS\ComponentManager.h"
#include "ECS\SystemManager.h"
#include "ECS\MessageData.h"

#endif
